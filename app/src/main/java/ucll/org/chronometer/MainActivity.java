package ucll.org.chronometer;

import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private Button startStopButton;
    private long accumulatedMilliseconds = 0;
    private long delay = 0;
    private long pausedTime = System.currentTimeMillis();
    private long start = System.currentTimeMillis();
    private Handler handler;
    private boolean running = false;
    private static final long tickRate = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler();
        textView = (TextView) findViewById(R.id.textView);
        startStopButton = (Button) findViewById(R.id.startStopButton);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        accumulatedMilliseconds = prefs.getLong("accumulatedMilliseconds", 0);
        delay = prefs.getLong("delay", 0);
        start = prefs.getLong("start", System.currentTimeMillis());
        pausedTime = prefs.getLong("pausedTime", System.currentTimeMillis());
        running = prefs.getBoolean("running", false);
        if (running) {
            scheduleNextTick();
            startStopButton.setText(getString(R.string.start_button_stop));
        } else {
            startStopButton.setText(getString(R.string.start_button_start));
        }
        updateTimeView();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("accumulatedMilliseconds", accumulatedMilliseconds);
        editor.putLong("pausedTime", pausedTime);
        editor.putLong("delay", delay);
        editor.putLong("start", start);
        editor.putBoolean("running", running);
        editor.apply();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void onTick() {
        increment();
        updateTimeView();
        scheduleNextTick();
    }

    private void scheduleNextTick() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onTick();
            }
        }, tickRate);
    }

    private void increment() {

        long inc = System.currentTimeMillis() - start - delay - accumulatedMilliseconds;
        accumulatedMilliseconds += inc;

    }

    private void reset() {
        start = System.currentTimeMillis();
        accumulatedMilliseconds = 0;
        delay = 0;
        pausedTime = System.currentTimeMillis();
    }

    private void updateTimeView() {
        Date date = new Date(accumulatedMilliseconds);
        DateFormat formatter = new SimpleDateFormat("mm:ss:SS");

        textView.setText(formatter.format(date));
    }

    public void onStartStopClicked(View view) {
        if (!running) {
            delay += System.currentTimeMillis() - pausedTime;
            scheduleNextTick();
            startStopButton.setText(R.string.start_button_stop);
            running = true;
        } else {
            pausedTime = System.currentTimeMillis();
            handler.removeCallbacksAndMessages(null);
            startStopButton.setText(R.string.start_button_start);
            running = false;
        }
    }

    public void onResetClicked(View view) {
        reset();
        updateTimeView();
    }
}
